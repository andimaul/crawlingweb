<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Crawler System</title>

    <!-- Icons-->
    <link href="css/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
    <link href="css/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
    <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Main styles for this {application-->
    <link href="{{ asset('css/style.min.css') }}" rel="stylesheet">}

    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> -->

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"> -->
    <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" >
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet">

    @yield('styles')
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

    <header class="app-header navbar">
        <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">
            <img class="navbar-brand-full" src="img/logo.svg" width="89" height="25" alt="CoreUI Logo">
            <img class="navbar-brand-minimized" src="img/sygnet.svg" width="30" height="30" alt="CoreUI Logo">
        </a>
        <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
            <span class="navbar-toggler-icon"></span>
        </button>
        <ul class="nav navbar-nav ml-auto" style="padding: 0.75rem 1rem;">
            
        </ul>
    </header>

    <div class="app-body" id="app">
        <div class="sidebar">
            <nav class="sidebar-nav">
                <ul class="nav">
                    <li class="nav-item">
                        <li class="nav-title">Menu</li>
                        <a class="nav-link" href="home">
                            <i class="nav-icon icon-grid"></i> Overview
                        </a>

                    </li>

                    <li class="nav-title">SETTING</li>
                    <li class="nav-item">
                        <a class="nav-link" href="crawl">
                            <i class="nav-icon icon-cup icons"></i> Crawl</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="logout">
                            <i class="nav-icon icon-logout icons"></i> Logout</a>
                    </li>

                </ul>

                <ul class="nav side-menu">

            </nav>
            <button class="sidebar-minimizer brand-minimizer" type="button"></button>
        </div>
        <main class="main" style="background-color: #f5f5f5 !important;">
            <!-- Breadcrumb-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Home</li>
                <li class="breadcrumb-item">
                    <a href="#">Admin</a>
                </li>
                <li class="breadcrumb-item active">Dashboard</li>

            </ol>
            <div class="container-fluid">
                <div class="animated fadeIn">
                    @yield('content')
                </div>
            </div>
        </main>

    </div>

    <footer class="app-footer">
        <div>
            <a href="{{'/'}}">Crawler v0.0.1</a>
            <span>&copy; <script>document.write(new Date().getFullYear());</script></span>
        </div>
    </footer>
</body>

    <!-- Scripts -->

    <script>
        function _cacheScript(c, a, b) {
            var d = new XMLHttpRequest();
            d.onreadystatechange = function() {
                if (d.readyState == 4) {
                    if (d.status == 200) {
                        localStorage.setItem(c, JSON.stringify({
                            content: d.responseText,
                            version: a
                        }))
                    } else {
                        console.warn("error loading " + b)
                    }
                }
            };
            d.open("GET", b, true);
            d.send()
        }

        function _loadScript(c, b, a, e) {
            var d = document.createElement("script");
            if (d.readyState) {
                d.onreadystatechange = function() {
                    if (d.readyState == "loaded" || d.readyState == "complete") {
                        d.onreadystatechange = null;
                        _cacheScript(b, a, c);
                        if (e) {
                            e()
                        }
                    }
                }
            } else {
                d.onload = function() {
                    _cacheScript(b, a, c);
                    if (e) {
                        e()
                    }
                }
            }
            d.setAttribute("src", c);
            document.getElementsByTagName("head")[0].appendChild(d)
        }

        function _injectScript(g, e, d, b, i) {
            var h = JSON.parse(g);
            if (h.version != b) {
                localStorage.removeItem(d);
                _loadScript(e, d, b, i);
                return
            }
            var f = document.createElement("script");
            f.type = "text/javascript";
            var a = document.createTextNode(h.content);
            f.appendChild(a);
            document.getElementsByTagName("head")[0].appendChild(f);
            if (i) {
                i()
            }
        }

        function requireScript(d, a, b, f) {
            var e = localStorage.getItem(d);
            if (e == null) {
                _loadScript(b, d, a, f)
            } else {
                _injectScript(e, b, d, a, f)
            }
        };

        requireScript('jquery', '3.3.1', 'https://code.jquery.com/jquery-3.3.1.min.js')
        requireScript('popper', '1.14.6', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js')
        requireScript('bootstrap', '4.1.3', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js')
        requireScript('pace', '1.0.2', 'https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js')
        requireScript('perfect-scrollbar', '1.4.0', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/1.4.0/perfect-scrollbar.min.js')
    </script>


    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <!-- <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
    <!-- <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script> -->
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>



 <script type="text/javascript">
        $(document).ready(function() {
            $(".select2_market").select2({
                placeholder: "Select Marketplace",
                allowClear: true
            });
            $(".select2_kategori").select2({
                placeholder: "Select Category",
                allowClear: true
            });
            $(".select2_sub").select2({
                placeholder: "Select SubCategory",
                allowClear: true
            });
            $(".select2_detail").select2({
                placeholder: "Select SubCategoryDetail",
                allowClear: true
            });
            $(".select2_instruction").select2({
                placeholder: "Select Marketplace",
                allowClear: true
            });
            $(".select2_page").select2({
                placeholder: "Select Max Page",
                allowClear: true
            });
        });
    </script>
   @yield('scripts')

</html>
