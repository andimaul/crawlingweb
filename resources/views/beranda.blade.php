@extends('layouts.master')

@section('content')
<div class="row">
    <div class="col-3 col-lg-3">
        <div class="card">
            <div class="card-header">
                Filter
            </div>
            <div class="card-body">
                <form method="get" id="form-data" action="{{route('filter')}}">
                    {{ csrf_field() }}
                    <div class="form-group">

                        <select class="select2_market js-states form-control col-md-12 dynamic" id="marketplace" name="marketplace" data-dependent="kategori">
                            <option></option>

                            @foreach ($produk as $market)
                            <option value="{{$market->marketplace}}"> {{$market->marketplace}}</option>
                            @endforeach

                        </select>
                    </div>
                    <div class="form-group">
                        <select class="select2_kategori js-states form-control col-md-12 dynamic" id="kategori" name="kategori" data-dependent="subkategori">
                            <option></option>

                        </select>
                    </div>
                    <div class="form-group">
                        <select class="select2_sub js-states form-control col-md-12 dynamic" id="subkategori" name="sub_kategori" data-dependent="subdetail">
                            <option></option>

                        </select>
                    </div>
                    <div class="form-group">
                        <select class="select2_detail js-states form-control col-md-12" id="subdetail" name="sub_detail">
                            <option></option>

                        </select>
                    </div>

                    <button type="submit" class="btn btn-primary" id="submit"> <i class="icon-fire icons "></i> Apply</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-9 col-lg-9">
        <div class="row">
            <div class="col-9 col-lg-6">
                <div class="card">
                    <div class="card-body p-3 d-flex align-items-center">
                        <i class="fa fa-arrow-circle-up bg-success p-3 font-2xl mr-1"></i>
                        <div>
                            <div class="text-muted text-uppercase font-weight-bold small">Max Price</div>
                            <div class="text-value-sm text-primary" style="font-size:13px;" id="card-high-price">

                                <!-- MAX HARGA -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3 col-lg-3">
                <div class="card">
                    <div class="card-body p-3 d-flex align-items-center">
                        <i class="fa fa-arrow-circle-down bg-danger p-3 font-2xl mr-1"></i>
                        <div>
                            <div class="text-muted text-uppercase font-weight-bold small">Min Price</div>
                            <div class="text-value-sm text-primary" id="card-min-price" style="font-size:14px;">
                                <!-- MIN HARGA -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-3 col-lg-3">
                <div class="card">
                    <div class="card-body p-3 d-flex align-items-center">
                        <i class="fa fa-th bg-primary p-3 font-2xl mr-1"></i>
                        <div>
                            <div class="text-muted text-uppercase font-weight-bold small">All Product</div>
                            <div class="text-value-sm text-primary" id="total-product" style="font-size:14px;">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                Data &nbsp;
            </div>
            <div class="card-body">
                <!-- <div class="text-center" id="empty">
                    <img src="img/empty-box.png" width="100px" height="100px">
                </div> -->
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="myTab" data-toggle="tab" href="#bestSeller" role="tab" aria-controls="home" aria-selected="true">All store</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="best-tab" data-toggle="tab" href="#bestStore" role="tab" aria-controls="profile" aria-selected="false">Best Store</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="conver-tab" data-toggle="tab" href="#home" role="tab" aria-controls="profile" aria-selected="flase">Conversion Rate</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="location-tab" data-toggle="tab" href="#store" role="tab" aria-controls="profile" aria-selected="false">Store Location</a>
                    </li>
                </ul>
                <div class="tab-content" id="TabContent">
                    <div class="tab-pane fade show active" id="bestSeller" role="tabpanel" aria-labelledby="home-tab">

                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th scope="col">Store</th>
                                    <th scope="col">Image</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Rating</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Sales</th>
                                    <th scope="col">Date</th>

                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="bestStore" role="tabpanel" aria-labelledby="profile-tab">
                        <table id="table-store" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th scope="col">Store</th>
                                    <th scope="col">Rating</th>
                                    <th scope="col">Location</th>
                                    <th scope="col">Produk</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Sales</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>

                        </table>

                    </div>
                    <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="profile-tab">
                        <canvas id="rateChart" width="80" height="80"></canvas>
                    </div>
                    <div class="tab-pane fade" id="store" role="tabpanel" aria-labelledby="profile-tab">
                        <canvas id="storeChart" width="80" height="80"></canvas>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection

@section('scripts')

<script>
    $(document).ready(function(){
        filterSelect();
        $('#marketplace').change(function() {
            $('#kategori').val('');
            $('#subkategori').val('');
            $('#subdetail').val('');
        });

        $('#kategori').change(function() {
            $('#subkategori').val('');
            $('#subdetail').val('');
        });

        $('#subkategori').change(function() {
            $('#subdetail').val('');
        });

        $(document).on('change','#marketplace',function(){
            if($(this).val() != ''){
                var select = $(this).attr("name");
                var value = $(this).val();
            }
            $.ajax({
                url:'{{ url("/home/select/kategori") }}',
                type:'get',
                data:{
                    'select':select,
                    'value':value,
                },
                success:function(response){
                   //console.log(response);
                $('#kategori').empty();
                $('#subkategori').empty();
                $('#subdetail').empty();
                $('#kategori').append(`<option></option>`);
                $('#subkategori').append(`<option></option>`);
                $('#subdetail').append(`<option></option>`);
                   var kategori = response.data;
                  //console.log(kategori);

                  for(var i = 0; i < kategori.length; i++){
                    $('#kategori').append(`<option value="${kategori[i].kategori}">${kategori[i].kategori}`);
                  }
                }
            });
        });


        $(document).on('change', '#kategori', function() {
            if ($(this).val() != '') {
                var select = $(this).attr("name");
                var value = $(this).val();

            }
            $.ajax({
                url: '{{ url("/home/select/kategori/subkategori")}}',
                type: 'get',
                data: {
                    'select': select,
                    'value': value,
                },
                success: function(response) {
                    $('#subkategori').empty('');
                    $('#subdetail').empty('');
                    $('#subkategori').append(`<option></option>`);
                    $('#subdetail').append(`<option></option>`);
                    // console.log(response);
                    var subkategori = response.data;
                    console.log(subkategori);
                    for (var i = 0; i < subkategori.length; i++) {
                        $('#subkategori').append(`<option value="${subkategori[i].sub_kategori}">${subkategori[i].sub_kategori}`);
                    }


                }
            });
        });

      $(document).on('change', '#subkategori', function() {
         if ($(this).val() != '') {
             var select = $(this).attr("name");
             var value = $(this).val();

         }
         $.ajax({
             url: '{{ url("/home/select/kategori/subkategori/subdetail")}}',
             type: 'get',
             data: {
                 'select': select,
                 'value': value,
             },
             success: function(response) {
                 // console.log(response);
                 $('#subdetail').empty('');
                 $('#subdetail').append(`<option></option>`);
                 var subdetail = response.data;
                 console.log(subdetail);
                 for (var i = 0; i < subdetail.length; i++) {
                     $('#subdetail').append(`<option value="${subdetail[i].sub_detail}">${subdetail[i].sub_detail}`);
                 }


             }
         });
      });

      //chart lokasi store
      function chartLokasi(data, labels) {
          $('#storeChart').remove()
          $('#store').append('<canvas id="storeChart" width="80" height="80"></canvas>')
          var storeChart = document.getElementById('storeChart');
          var myDoughnutChart = new Chart(storeChart, {
              type: 'doughnut',
              data: {
                  labels: labels,
                  datasets: [{
                      data: data,
                      label: labels,
                      backgroundColor: [
                          'rgba(255, 99, 132, 1)',
                          'rgba(54, 162, 235, 1)',
                          'rgba(255, 206, 86, 1)',
                          'rgba(75, 192, 192, 1)',
                          'rgba(153, 102, 255, 1)',
                          'rgba(255, 159, 64,1)',
                          'rgba(0, 159, 64,1)',
                          'rgba(255, 0, 64,1)',
                          'rgba(255, 159, 0,1)',

                      ],
                      borderColor: [
                          'rgba(255, 99, 132, 1)',
                          'rgba(54, 162, 235, 1)',
                          'rgba(255, 206, 86, 1)',
                          'rgba(75, 192, 192, 1)',
                          'rgba(153, 102, 255, 1)',
                          'rgba(255, 159, 64, 1)'
                      ],
                  }]
              },
              options: {}
          })
      }

      //chart conversion product
      function chartConvert(data, labels) {
          $('#rateChart').remove()
          $('#home').append('<canvas id="rateChart" width="80" height="80"></canvas>')
          var convertChart = document.getElementById('rateChart');
          var myLineChart = new Chart(convertChart, {
              type: 'bar',
              data: {
                  labels: labels,
                  datasets: [{
                      label: 'Conversion',
                      fill: false,
                      data: data,
                      backgroundColor: [
                          'rgba(255, 29, 111, 1)',
                          'rgba(54, 162, 235, 1)',
                          'rgba(255, 206, 86, 1)',
                          'rgba(75, 192, 192, 1)',
                          'rgba(153, 102, 255, 1)'

                      ],
                      borderColor: [
                          'rgba(255, 99, 132, 1)',
                          'rgba(54, 162, 235, 1)',
                          'rgba(255, 206, 86, 1)',
                          'rgba(75, 192, 192, 1)',
                          'rgba(153, 102, 255, 1)'

                      ],
                  }]
              },
              options: {}
          })
      }
      //end chart conversion product

      function convertToRupiah(angka) {
          var rupiah = '';
          var angkarev = angka.toString().split('').reverse().join('');
          for (var i = 0; i < angkarev.length; i++)
              if (i % 3 == 0)
                  rupiah += angkarev.substr(i, 3) + '.';
          // console.log(rupiah);
          var a = 'Rp. ' + rupiah.split('', rupiah.length - 1).reverse().join('');
          return a;
      }

      function convertTotalProduct(angka) {
          var rupiah = '';
          var angkarev = angka.toString().split('').reverse().join('');
          for (var i = 0; i < angkarev.length; i++)
              if (i % 3 == 0)
                  rupiah += angkarev.substr(i, 3) + '.';
          // console.log(rupiah);
          var a = rupiah.split('', rupiah.length - 1).reverse().join('');
          return a;
      }

      //ajax call rating
      function ajaxCall3() {
          $.ajax({
              url: '{{ url("/home/rate")}}',
              type: 'GET',
              dataType: 'json',
              data: {
                  'sub_detail': $('#subdetail').val()
              },
              success: function(response) {
                  //console.log(response);
                  var rate = response.rate;
                  var labels = ['★★★★★', '★★★★', '★★★', '★★', '★', '0 rate'];

                  var data = [];
                  for (let i = 0; i < rate.length; i++) {
                      data.push(rate[i].jumlah)
                  };

                  //chart total product
                  // chartRate(data,labels);
                  ajaxCall4();

              }
          })
      }
      //ajax call chartlokasi
      function ajaxCall4() {
          $.ajax({
              url: '{{ url("/home/lokasi") }}',
              type: 'GET',
              datatype: 'json',
              data: {
                  'sub_detail': $('#subdetail').val()
              },
              success: function(response) {

                  // console.log(response);
                  var lokasi = response.data;
                  var labels = [];
                  console.log(lokasi[0].lokasi_toko);
                  for (let i = 0; i < lokasi.length; i++) {
                      labels.push(lokasi[i].lokasi_toko)
                  };
                  // console.log(labels);
                  var data = [];
                  for (let i = 0; i < lokasi.length; i++) {
                      data.push(lokasi[i].jumlah)
                  };
                  // console.log(data);
                  chartLokasi(data, labels);
                  ajaxCall5();
              }
          })
      }
      //ajax call highprice
      function ajaxCall5() {
          $.ajax({
              url: '{{url("/home/price/high")}}',
              type: 'GET',
              dataType: 'json',
              data: {
                  'sub_detail': $('#subdetail').val()
              },
              success: function(response) {
                  console.log(response);
                  $('#card-high-price').empty();
                  let price = response.price;
                  for (let i = 0; i < price.length; i++) {
                      let angka = price[i].harga
                      $('#card-high-price').append(`${convertToRupiah(angka)}<br>${price[i].produk}<br><a href="${price[i].url_toko}">${price[i].nama_toko}</a><br><img src=${price[i].rating_toko}><br>${price[i].lokasi_toko}`);
                  }
                  ajaxCal6();
              }
          })
      }

      function ajaxCal6() {
          $.ajax({
              url: '{{ url("/home/price/min") }}',
              type: 'GET',
              dataType: 'json',
              data: {
                  'sub_detail': $('#subdetail').val()
              },
              success: function(response) {
                  // console.log(response)
                  $('#card-min-price').empty();
                  let price = response.price;
                  for (var i = 0; i < price.length; i++) {
                      let angka = price[i].harga
                      $('#card-min-price').append(convertToRupiah(angka));
                  }
                  callAjax7();
              }
          })
      }

      function callAjax7() {
          $.ajax({
              url: '{{ url("/home/total") }}',
              type: 'GET',
              dataType: 'json',
              data: {
                  'sub_detail': $('#subdetail').val()
              },
              success: function(response) {
                  // console.log(response);
                  $('#total-product').empty();
                  let total = response.total
                  for (let i = 0; i < total.length; i++) {
                      let angka = total[i].total_produk
                      $('#total-product').append(convertTotalProduct(angka));
                  };
              }

          })
      }

      function bestStore() {
        event.preventDefault();
        $.ajax({
            url: "{{ url('/home/select/best-store') }}",
            type: 'GET',
            dataType: 'json',
            data: {
                'sub_detail': $('#subdetail').val()
            },
            success: function(response) {
                // console.log(response);
                let store = response.bestStore;
                $('#table-store').DataTable().clear().draw();
                $('#table-store').DataTable().destroy();
                for (let i = 0; i < store.length; i++) {
                    let angka = store[i].harga_produk;
                    $('#table-store tbody').append(`<tr><td><a href="${store[i].url_toko}">${store[i].nama_toko}</a></td><td><img src="${store[i].rating_toko}"></td><td>${store[i].lokasi_toko}</td><td>${store[i].produk}</td><td>` + convertToRupiah(angka) + `</td><td>${store[i].terjual}</td></tr>`);
                }
                $('#table-store').DataTable({
                    "searching": false,
                    "paging": false,
                    "aaSorting": [5, 'desc'],
                })
            }
        })
      }


      //ajax call table
      function filterSelect() {
          $('#example').hide()
          $('#submit').click(function(event) {
              event.preventDefault();
              $.ajax({
                  url: '{{ url("/home/select") }}',
                  type: 'GET',
                  dataType: 'json',
                  data: {
                      'sub_kategori': $('#subkategori').val(),
                      'sub_detail': $('#subdetail').val(),
                  },
                  success: function(response) {
                      $('#empty').hide();
                      $('#example').DataTable().clear().draw();
                      $('#example').DataTable().destroy();
                      // console.log(response)
                      let listproduk = response.listproduk;
                      for (let i = 0; i < listproduk.length; i++) {
                          let angka = `${listproduk[i].harga_produk}`;
                          $('#example tbody').append(`<tr><td><a href="${listproduk[i].url_toko}">${listproduk[i].nama_toko}</a></td><td><img src=" ${listproduk[i].url_gambar} " style="width:60px;height:60px;"></td><td>${listproduk[i].produk}</td><td>${listproduk[i].rate}</td><td>` + convertToRupiah(angka) + `</td><td>${listproduk[i].terjual}</td><td>${listproduk[i].tanggal}</td></tr>`);
                      }
                      $('#example').show()
                      $('#example').DataTable({
                        "language": {
                            "emptyTable": "<img src='/img/stocked.png' style='width: 400px;'><br> "+
                                                '<h5>Belum ada barang dijual</h5>'+
                                                '<p>Barang jualan kamu akan muncul di halaman ini. Ayo mulai berjualan sekarang!</p>'
                          }
                      });
                      conversionRate();
                      bestStore()
                  }

              })
          });
      }

      //ajax call conversion
      function conversionRate() {
          $.ajax({
              url: '{{ url("/home/convert")}}',
              type: 'GET',
              dataType: 'json',
              data: {
                  'sub_detail': $('#subdetail').val()
              },
              success: function(response) {
                  let convert = response.convert
                  var data = [];
                  for (let i = 1; i < convert.length; i++) {
                      let a = convert[i].total_dilihat - convert[i - 1].total_dilihat;
                      let b = convert[i].total_transaksi - convert[i - 1].total_transaksi;
                      let c = (b / a) * 100 / 100
                      let d = c.toFixed(4);
                      data.push(d);
                  };
                  var labels = [];
                  for (let i = 1; i < convert.length; i++) {
                      labels.push(convert[i].tanggal)
                  };
                  chartConvert(data, labels);

                  ajaxCall3();

              }
          });
      }

    })





</script>


@endsection
