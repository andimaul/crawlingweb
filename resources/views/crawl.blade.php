@extends('layouts.master')

@section('content')
 <div class="card-body col-3 col-lg-3">
                    <!-- <div class="text-center" id="empty">
                        <img src="img/empty-box.png" width="100px" height="100px">
                    </div> -->
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="myTab" data-toggle="tab" href="#Kategori" role="tab" aria-controls="home" aria-selected="true">Kategori</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="produk-tab" data-toggle="tab" href="#Produk" role="tab" aria-controls="profile" aria-selected="false">Produk</a>
                        </li>
                        <li class="nav-item">
                                <a class="nav-link" id="Store-tab" data-toggle="tab" href="#Store" role="tab" aria-controls="profile" aria-selected="flase">Store</a>
                        </li>
                         <li class="nav-item">
                                <a class="nav-link" id="keyword-tab" data-toggle="tab" href="#Keyword" role="tab" aria-controls="profile" aria-selected="false">Keyword</a>
                            </li>
                    </ul>

                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="Kategori" role="tabpanel" aria-labelledby="home-tab">
                          <form method="post" id="form-crawl-market-category" action="{{route ('crawlingcategory')}}">
                            {{ csrf_field() }}

                              <div class="form-group" id="marketplaceCategory">
                                              <div class="form-group">
                                                <small id="select" class="form-text text-muted">Select Marketplace.</small>
                                                <select class="select2_market js-states form-control col-md-10 dynamic" id="marketplace" name="marketplace" data-dependent="kategori">
                                                    <option></option>

                                                    @foreach ($produk as $market)
                                                    <option value="{{$market->marketplace}}"> {{$market->marketplace}}</option>
                                                    @endforeach

                                                </select>
                                              </div>
                                            <div class="form-group">
                                                <select class="select2_kategori js-states form-control col-md-10 dynamic" id="kategori" name="kategori" data-dependent="subkategori">
                                                    <option></option>

                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <select class="select2_sub js-states form-control col-md-10 dynamic" id="subkategori" name="sub_kategori" data-dependent="subdetail">
                                                    <option></option>

                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <select class="select2_detail js-states form-control col-md-10" id="subdetail" name="sub_detail">
                                                    <option></option>

                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <select class="select2_page js-states form-control col-md-10" id="page" name="page">
                                                    <option></option>
                                                    @foreach ($page as $page)
                                                    <option value="{{$page->page}}">{{$page->page}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                  </div>
                                <button type="submit" class="btn btn-light" id="button-market-category">Run</button>
                                </form> 
                                           
                            </div>


                      
                      <div class="tab-pane fade" id="Produk" role="tabpanel" aria-labelledby="profile-tab">
                         <form method="post" id="form-crawl-produk" action="">
                          {{ csrf_field() }}
                        <div class="form-group" id="product">
                                    <label for="formGroupExampleInput2" >Product Link</label>
                                    <input type="text" class="form-control"  placeholder="Add Product link" id="productlinkinput" name="productlinkinput">
                                    
                        </div>
                            <button type="submit" class="btn btn-light" id="button-produk">Run</button>
                            </form>
                       </div>

                      <div class="tab-pane fade" id="Store" role="tabpanel" aria-labelledby="contact-tab"> 
                        <form method="post" id="form-crawl-produk" action="{{route('crawlingstore')}}">
                          {{ csrf_field() }}
                        <div class="form-group" id="storelink" name="storelink">
                                    <label >Store Link</label>
                                    <input type="text" class="form-control"  placeholder="Add Store link" id="storelinkinput" name="storelinkinput">
                            </div>
                            <button type="submit" class="btn btn-light" id="button-store">Run</button>
                            </form>
                          </div>
                      <div class="tab-pane fade" id="Keyword" role="tabpanel" aria-labelledby="contact-tab">
                        <form method="post" id="form-crawl-keyword" action="{{route('crawlingkeyword')}}">
                          {{ csrf_field() }}
                        <div class="form-group" id="keywords">
                                    <label for="formGroupExampleInput2" >Keywords</label>
                                    <input type="text" class="form-control"  placeholder="Add Keywords" id="keywordinput" name="keywordinput" >
                            </div>

                        <div class="form-group">
                                                <select class="select2_page js-states form-control col-md-10" id="page_keyword" name="page_keyword">
                                                    <option></option>
                                                    @foreach ($page_keyword as $page_keyword)
                                                    <option value="{{$page_keyword->page}}">{{$page_keyword->page}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                            <button type="submit" class="btn btn-light" id="button-keyword">Run</button>
                            </form>
                      </div>
                      
                    </div>

</div>
@endsection

@section('scripts')
<script>
//   $(document).ready(function(){

//   $('#storelink').hide();
//   $('#keywords').hide();
//   $('#button').hide();
//   $('#product').hide();
//  $('#marketplaceCategory').hide();
//  });



//   $(document).ready(function(){
//     $(document).on('change','#instruction',function(){
//       event.preventDefault();
//       var action = "{{ route('crawlingstore') }}";
//       if($(this).val()=='Add by store link'){
//         // $('#form-crawl').attr('action');
//         $('#form-crawl').attr('action',action);
//         $('#storelink').show();
//           $('#keywords').hide();
//           $('#product').hide();
//           $('#button').show();
//           $('#marketplaceCategory').hide();

//       }

//       $('#button').click(function(){
//         // event.preventDefault(); 
//         $.ajax({
//             url: '/crawl/crawling',
//             type: 'POST',
//             dataType: 'string',
//              headers: {
//     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//   },
//             data: {
//                 'storelinkinput' : $('#storelinkinput').val()
//             },
//             success: function(response){
//                  if(response === "no_errors") top.location.href= "crawl.blade.php"
//                 }
//     })
//       })
//       $.ajaxSetup({
 
// });
      
//       })
//   });

//  $(document).ready(function(){
//     $(document).on('change','#instruction',function(){
      
//       if($(this).val()=='Add by keyword'){
    
//         $('#form-crawl').attr('action',action);
//         $('#storelink').hide();
//         $('#keywords').show();
//         $('#product').hide();
//          $('#button').show();
//          $('#marketplaceCategory').hide();
//          ;
//       }
//     })
//   });
// $(document).ready(function(){
//     $(document).on('change','#instruction',function(){
//       if($(this).val()=='Add by product link'){
//           $('#storelink').hide();
//          $('#keywords').hide();
//           $('#product').show();
//            $('#button').show();
//            $('#marketplaceCategory').hide();
//          }
//     })
//   });

// $(document).ready(function(){
//     $(document).on('change','#instruction',function(){
//       if($(this).val()=='Add by marketplace category'){
//           $('#storelink').hide();
//          $('#keywords').hide();
//           $('#product').hide();
//           $('#marketplaceCategory').show();
//            $('#button').show();
//          }
//     })
//   });




$('#marketplace').change(function(){
  $('#kategori').val('');
  $('#subkategori').val('');
  $('#subdetail').val('');
  $('#page').val('');
 });


 $('#kategori').change(function(){
  $('#subkategori').val('');
  $('#subdetail').val('');
  $('#page').val('');
 });
   $('#subkategori').change(function(){
    $('#subdetail').val('');
    $('#page').val('');
   });

    $(document).ready(function(){
        $(document).on('change','#marketplace',function(){
           
            if($(this).val() != ''){
                var select = $(this).attr("name");
                var value = $(this).val();

            }
            $.ajax({
                url:'/crawl/select/kategori',
                type:'get',
                data:{
                    'select':select, 
                    'value':value, 
                },
                success:function(response){
                   console.log(response);
                $('#kategori').empty();
                $('#subkategori').empty();
                $('#subdetail').empty();
                $('#kategori').append(`<option></option>`);
                $('#subkategori').append(`<option></option>`);
                $('#subdetail').append(`<option></option>`);
                   var kategori = response.kategori;
                  console.log(kategori);

                  for(var i = 0; i < kategori.length; i++){
                    $('#kategori').append(`<option value="${kategori[i].kategori}">${kategori[i].kategori}`);
                  }
                   
                   var kategori = $('#kategori').val()
                  
                }
            });
        })
    })


    $(document).ready(function(){
        $(document).on('change','#kategori',function(){
              if($(this).val() != ''){
                var select = $(this).attr("name");
                var value = $(this).val();

            }
            $.ajax({
                url:'crawl/select/kategori/subkategori',
                type:'get',
                data:{
                    'select':select, 
                    'value':value, 
                },
                success:function(response){
                    $('#subkategori').empty('');
                    $('#subdetail').empty('');
                    $('#subkategori').append(`<option></option>`);
                    $('#subdetail').append(`<option></option>`);
                   console.log(response);
                   var subkategori = response.sub_kategori;
                   console.log(subkategori);
                   for(var i = 0; i < subkategori.length ; i++){
                     $('#subkategori').append(`<option value="${subkategori[i].sub_kategori}">${subkategori[i].sub_kategori}`);
                   }
                  
                  
                }
            }); 

                  
                })
            });

    $(document).ready(function(){
        $(document).on('change','#subkategori',function(){
              if($(this).val() != ''){
                var select = $(this).attr("name");
                var value = $(this).val();

            }
            $.ajax({
                url:'/crawl/select/kategori/subkategori/subdetail',
                type:'get',
                data:{
                    'select':select, 
                    'value':value, 
                },
                success:function(response){
                   // console.log(response);
                    $('#subdetail').empty('');
                    $('#subdetail').append(`<option></option>`);
                   var subdetail = response.subdetail;
                   console.log(subdetail);
                   for(var i = 0; i < subdetail.length;i++){
                    $('#subdetail').append(`<option value="${subdetail[i].sub_detail}">${subdetail[i].sub_detail}`);
                   }
                   
                  
                }
            }); 

                  
                })
            });
    // // $(document).ready(function(){
    //     $(document).on('change','#instruction',function(){
           
    //         if($(this).val() != ''){
    //             var select = $(this).attr("name");
    //             var value = $(this).val();

    //         }
    //         $.ajax({
    //             url:'/crawl/select/kategori',
    //             type:'get',
    //             data:{
    //                 'select':select, 
    //                 'value':value, 
    //             },
    //             success:function(response){
    //                //console.log(response);
    //             $('#kategori').empty();
    //             $('#subkategori').empty();
    //             $('#subdetail').empty();
    //             $('#kategori').append(`<option></option>`);
    //             $('#subkategori').append(`<option></option>`);
    //             $('#subdetail').append(`<option></option>`);
    //                var kategori = response.data;
    //               //console.log(kategori);

    //               for(var i = 0; i < kategori.length; i++){
    //                 $('#kategori').append(`<option value="${kategori[i].kategori}">${kategori[i].kategori}`);
    //               }
                   
    //                //var kategori = $('#kategori').val()
                  
    //             }
    //         });
    //     })
    // })


    // $(document).ready(function(){
    //     $(document).on('change','#kategori',function(){
    //           if($(this).val() != ''){
    //             var select = $(this).attr("name");
    //             var value = $(this).val();

    //         }
    //         $.ajax({
    //             url:'/crawl/select/kategori/subkategori',
    //             type:'get',
    //             data:{
    //                 'select':select, 
    //                 'value':value, 
    //             },
    //             success:function(response){
    //                 $('#subkategori').empty('');
    //                 $('#subdetail').empty('');
    //                 $('#subkategori').append(`<option></option>`);
    //                 $('#subdetail').append(`<option></option>`);
    //                // console.log(response);
    //                var subkategori = response.data;
    //                console.log(subkategori);
    //                for(var i = 0; i < subkategori.length ; i++){
    //                  $('#subkategori').append(`<option value="${subkategori[i].sub_kategori}">${subkategori[i].sub_kategori}`);
    //                }
                  
                  
    //             }
    //         }); 

                  
    //             })
    //         });

    // $(document).ready(function(){
    //     $(document).on('change','#subkategori',function(){
    //           if($(this).val() != ''){
    //             var select = $(this).attr("name");
    //             var value = $(this).val();

    //         }
    //         $.ajax({
    //             url:'/crawl/select/kategori/subkategori/subdetail',
    //             type:'get',
    //             data:{
    //                 'select':select, 
    //                 'value':value, 
    //             },
    //             success:function(response){
    //                // console.log(response);
    //                 $('#subdetail').empty('');
    //                 $('#subdetail').append(`<option></option>`);
    //                var subdetail = response.data;
    //                console.log(subdetail);
    //                for(var i = 0; i < subdetail.length;i++){
    //                 $('#subdetail').append(`<option value="${subdetail[i].sub_detail}">${subdetail[i].sub_detail}`);
    //                }
                   
                  
    //             }
    //         }); 

                  
    //             })
    //         });
</script>
    
@endsection
