<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageCrawling extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'page_crawling';
}
