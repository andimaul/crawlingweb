<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class crawlingOptions extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'crawler_instruction';
}
