<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subdetail extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sub_kategori_detail';
}
