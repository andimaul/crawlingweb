<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class BerandaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)

    {
        $marketplace = \App\Produk::select('marketplace')->distinct()->get();
        $kategori = \App\Kategori::select('kategori')->where(['marketplace'=>$request->marketplace])->distinct()->get();
        $subkategori = \App\Produk::select('sub_kategori')->distinct()->get();
        $subdetail = \App\Produk::select('sub_detail')->distinct()->get();
        $listproduk = '';
        $maxharga = '';



         return view('beranda')
        ->with(['produk'=>$marketplace]);


    }

    public function select(Request $request)

    {

        if ($request->sub_detail === 'No detail category'){
            $listkategori = \App\Produk::where(['sub_kategori'=>$request->sub_detail])->get();
             return response()->json(array('listproduk'=>$listkategori));
        }else{
            $listproduk = \App\Produk::where(
            ['sub_detail' => $request->sub_detail])->get();

        // dd($listproduk);
       return response()->json(array('listproduk'=>$listproduk));
        }

    }

    public function bestStore(Request $request)
    {
        $subdetail = \App\Produk::where(['sub_detail' => $request->sub_detail])
        ->select('sub_detail')->pluck('sub_detail')->first();

        $bestStore = DB::select('CALL getBestStore(?)',array($subdetail));
        // dd($bestStore);
        return response()->json(array('bestStore'=>$bestStore));
    }

    public function getRate(Request $request)

    {
        $subdetail = \App\Produk::where(['sub_detail' => $request->sub_detail])
        ->select('sub_detail')->pluck('sub_detail')->first();

        $rate = DB::select('CALL getRate(?)',array($subdetail));

        return response()->json(array('rate'=>$rate));
    }

    public function getLokasi(Request $request)

    {
        $subdetail = \App\Produk::where(['sub_detail'=> $request->sub_detail])
        ->select('sub_detail')->pluck('sub_detail')->first();

        $data = DB::select('CALL getLokasi(?)',array($subdetail));
        //dd($data);
        return response()->json(array('data'=>$data));
    }

    public function kategori(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $data = DB::table('produk')
            ->where($select,$value)
            ->select('kategori')
            ->distinct()
            ->get();

            // dd($data);
        return response()->json(array('data'=>$data));
 }

    public function subkategori(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $data = DB::table('produk')
            ->where($select,$value)
            ->select('sub_kategori')
            ->distinct()
            ->get();
//dd($data);
        return response()->json(array('data'=>$data));
    }

     public function subdetail(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $data = DB::table('produk')
            ->where($select,$value)
            ->select('sub_detail')
            ->distinct()
            ->get();
//dd($data);
        return response()->json(array('data'=>$data));
    }

    public function getHighPrice(Request $request)

    {
        $subdetail = \App\Produk::where(['sub_detail'=> $request->sub_detail])
        ->select('sub_detail')->pluck('sub_detail')->first();

        $price = DB::select('CALL getHighPrice(?)',array($subdetail));
        // dd($price);
        return response()->json(array('price'=>$price));
    }

    public function getMinPrice(Request $request)

    {
        $subdetail = \App\Produk::where(['sub_detail'=> $request->sub_detail])
        ->select('sub_detail')->pluck('sub_detail')->first();

        $price = DB::select('CALL getMinPrice(?)',array($subdetail));
        return response()->json(array('price'=>$price));
    }


    public function getTotalProduct(Request $request)

    {
        $subdetail = \App\Produk::where(['sub_detail'=> $request->sub_detail])
        ->select('sub_detail')->pluck('sub_detail')->first();

        $total = DB::select('CALL getTotalProduct(?)',array($subdetail));
        // dd($total);
        return response()->json(array('total'=>$total));
    }

    public function getConvert(Request $request)
    {
        $subdetail = \App\Produk::where(['sub_detail'=> $request->sub_detail])
        ->select('sub_detail')->pluck('sub_detail')->first();

        $convert = DB::select('CALL getConversion(?)',array($subdetail));

        return response()->json(array('convert'=>$convert));
    }
    public function crawl()

    {
        $crawlerOption = \App\crawlingOptions::select('instruction')->get();
        $marketplace = \App\Subdetail::select('marketplace')->distinct()->get();
        $page = \App\PageCrawling::select('page')->get();
        $page_keyword = \App\PageCrawling::select('page')->get();


        return view('crawl')
        ->with(['crawlerOption'=>$crawlerOption])
        ->with(['produk'=>$marketplace])
        ->with(['page'=>$page])
        ->with(['page_keyword'=>$page_keyword]);

    }

    public function crawlByStore(Request $request)
    {

        $storelink = $request->get('storelinkinput');
        // dd($storelink);
        DB::table('crawler')->insert([
            'url' =>$storelink
        ]);



        // dd("python " .app_path()."\Http\Controllers\python\storelink.py ".$storelink."/info");
        $result=exec("python " .app_path()."\Http\Controllers\python\storelink.py ".$storelink."/info");
        // dd($result);
        return redirect()->back();
        // ->with(['result'=>$result]);
        // return redirect('crawl');

    }
    public function crawlByKeyword(Request $request)
    {

        $keyword = $request->get('keywordinput');
        $page = $request->get('page_keyword');
        // $number = 12;
        // dd($keyword);
        DB::table('crawler')->insert([
            'keyword' =>$keyword
        ]);



        // dd("python " .app_path()."\Http\Controllers\python\pooltest.py "."https://www.tokopedia.com/search?q=".$keyword);
        $result=exec("python " .app_path()."\Http\Controllers\python\pool.py "."https://www.tokopedia.com/search?q=".$keyword." ".$page);

        return redirect()->back();
        // ->with(['result'=>$result]);
        // return redirect('crawl');

    }
    public function crawlByProduct(Request $request)
    {
        // $crawlerOption = \App\crawlingOptions::select('instruction')->get();
        // $marketplace = \App\Subdetail::select('marketplace')->distinct()->get();
        $keyword = $request->get('productlinkinput');


        DB::table('crawler')->insert([
            'url' =>$keyword
        ]);



        // dd("python " .app_path()."\Http\Controllers\python\testing.py ".$storelink." ".$number);
        $result=exec("python " .app_path()."\Http\Controllers\python\produklink.py ".$keyword." ".$number);
        // dd($result);
        return redirect()->back();
        // ->with(['result'=>$result]);
        // return redirect('crawl');

    }


    public function crawlByMarketCategory(Request $request)
    {
        // $crawlerOption = \App\crawlingOptions::select('instruction')->get();
        // $marketplace = \App\Subdetail::select('marketplace')->distinct()->get();
        $subdetail=$request->sub_detail;
        // dd($subdetail);
        if ($subdetail===null){
            $subkategori = \App\SubKategori::where(['sub_kategori'=>$request->sub_kategori])->select('url_sub_kategori')->get();
            // dd($subkategori);
            $page = $request->page;
            // dd($page);
            DB::table('crawler')->insert([
            'kategori' =>$subkategori
        ]);
            $result=exec("python " .app_path()."\Http\Controllers\python\pool.py ".$subkategori." ".$page);
            return redirect()->back();
        }else if (!empty($request->sub_detail)){

        $subdetail = \App\Subdetail::where(['sub_detail'=>$request->sub_detail])->select('url_sub_detail')->get();
        // $number = 1;
        // dd($subdetail);
        $page = $request->page;
        DB::table('crawler')->insert([
            'kategori' =>$subkategori
        ]);



        // dd("python " .app_path()."\Http\Controllers\python\testing.py ".$storelink." ".$number);
        $result=exec("python " .app_path()."\Http\Controllers\python\pool.py ".$subdetail." ".$page);
        // dd($result);
        return redirect()->back();
        };


        // ->with(['result'=>$result]);
        // return redirect('crawl');

    }

    public function selectKategori(Request $request){
       $select = $request->get('select');
        $value = $request->get('value');
        $data = DB::table('sub_kategori_detail')
            ->where($select,$value)
            ->select('kategori')
            ->distinct()
            ->get();
        return response()->json(array('kategori'=>$data));
    }

    public function selectSubKategori(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $data = DB::table('sub_kategori_detail')
            ->where($select,$value)
            ->select('sub_kategori')
            ->distinct()
            ->get();

        return response()->json(array('sub_kategori'=>$data));
    }

     public function selectSubKategoriDetail(Request $request)
    {
        $select = $request->get('select');
        $value = $request->get('value');
        $data = DB::table('sub_kategori_detail')
            ->where($select,$value)
            ->select('sub_detail')
            ->distinct()
            ->get();

        return response()->json(array('subdetail'=>$data));
    }

    public function logout(){
      Auth::logout();
      return redirect('/');
    }

}
