from multiprocessing import Pool
from selenium import webdriver
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.support import expected_conditions as EC 
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By 
import mysql
import sys, mysql.connector
import re
import time
from getalllink import setUp,link

def regex(produk):
        regexp = re.compile(r'rb')
        if regexp.search(produk):
            x = re.sub(r"rb\b","", produk)
            x = x.replace(',','.')
            a = float(x)*1000
            produk = (int(a))
            return produk
        else:
            return produk


def crawling(url):
	options = webdriver.ChromeOptions()
	options = options
	options.add_argument('--headless')
	options.add_argument("--log-level=3")
	driver = webdriver.Chrome(r'C:\GABI\TutorSelenium\tokpedmulti\chromedriver\chromedriver.exe',options=options)
	driver.implicitly_wait(30)
	driver.get(url)
	try:
		connection = mysql.connector.connect\
		(host = "localhost", user = "root", passwd ="maulana7", db = "tokopedia_crawl")
	except:
		print("No connection")
		sys.exit(0)
	cursor = connection.cursor()
	id_toko = driver.find_element_by_xpath('//input[@id="shop-id"]').get_attribute("value")
	print(id_toko)
	nama_toko = driver.find_element_by_xpath('//div[@class="pdp-shop__info__name-wrapper"]//span[@id="shop-name-info"]').text
	print(nama_toko)
	rating_toko = driver.find_element_by_xpath('//div[@class="pdp-shop__info__name-wrapper"]/img').get_attribute('src')
	print(rating_toko)
	link_toko = driver.find_element_by_xpath('//div[@class="pdp-shop__info__name-wrapper"]//a').get_attribute('href')
	print(link_toko)
	lokasi_toko = driver.find_element_by_xpath('//div[@class="pdp-shop__info"]//span[@itemprop="addressLocality"]').text
	print(lokasi_toko)
	id_produk = driver.find_element_by_xpath('//input[@id="product-id"]').get_attribute("value")
	print(id_produk)
	nama_produk = driver.find_element_by_xpath('//div[@class="rvm-left-column--right"]//h1[@class="rvm-product-title"]//span[@itemprop="name"]').text
	print(nama_produk)
	harga_produk = driver.find_element_by_xpath('//div[@class="rvm-price-holder mt-10"]//div[@class="rvm-price mr-15"]//span[@itemprop="price"]').text
	regexp = re.compile(r'.')
	if regexp.search(harga_produk):
	    x = re.sub(r"\B."r".\B","", harga_produk )
	    harga_produk  = x.replace('.','')
	    print(harga_produk)
	link_gambar = driver.find_element_by_xpath('//div[@class="content-img content-main-img product-detail__fixed-container"]/img').get_attribute('src')
	print(link_gambar)
	link_produk = driver.find_element_by_xpath('//input[@id="product-url"]').get_attribute("value")
	print(link_produk)
	kategori = driver.find_elements_by_xpath('//ul[@class="breadcrumb"]//span[@itemprop="name"]')[1].text
	print(kategori)
	sub_kategori = driver.find_elements_by_xpath('//ul[@class="breadcrumb"]//span[@itemprop="name"]')[2].text
	print(sub_kategori)
	try:
	    sub_detail = driver.find_elements_by_xpath('//ul[@class="breadcrumb"]//span[@itemprop="name"]')[3].text
	    print(sub_detail)
	except:
	    print('NoSuchElementException')
	ulasan = driver.find_element_by_xpath('//div[@class="rvm-product-transaction"]//span[@class="review-count"]').text
	ulasan = regex(ulasan)
	star = driver.find_elements_by_xpath('//span[contains(@class,"svg_icon__star_full")]')
	star_level = len(star)
	print(star_level)
	produk_terjual = driver.find_element_by_xpath('//span[@class="fw-600 all-transaction-count"]').text
	print("terjual" + produk_terjual)
	produk_terjual = regex(produk_terjual)
	dilihat = driver.find_element_by_xpath('//div[@class="rvm-product-info--item_value mt-5 view-count"]').text
	dilihat = regex(dilihat)
	transaksi_sukses = driver.find_element_by_xpath('//span[@class="fw-600 success-transaction-percent"]').text
	print(transaksi_sukses)
	cursor.execute('''INSERT INTO toko(id_toko,nama_toko,lokasi_toko,url_toko) VALUES (%s,%s,%s,%s) ON DUPLICATE KEY UPDATE id_toko=%s,nama_toko =%s, lokasi_toko=%s, url_toko=%s''',(id_toko,nama_toko,lokasi_toko,link_toko,id_toko,nama_toko,lokasi_toko,link_toko))
	connection.commit()
	cursor.execute('''INSERT INTO produk(nama_toko,rating_toko,url_toko,id_produk, produk, harga_produk, url_produk, url_gambar, id_toko, rate, terjual, transaksi_sukses, jumlah_ulasan,sub_kategori,sub_detail,kategori,dilihat,lokasi_toko)\
	VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)''',[nama_toko,rating_toko,link_toko,id_produk,nama_produk,harga_produk,link_produk,link_gambar,id_toko,star_level,produk_terjual,transaksi_sukses,ulasan,sub_kategori,sub_detail,kategori,dilihat,lokasi_toko])
	connection.commit()
	cursor.execute('''UPDATE toko SET marketplace = 'Tokopedia' WHERE url_toko like '%tokopedia%' ''')
	connection.commit()
	cursor.execute('''UPDATE produk SET marketplace = 'Tokopedia' WHERE url_produk like '%tokopedia%' ''')
	connection.commit()



if __name__ == '__main__':
	urls = link
	p = Pool(processes=5)
	result = p.map(crawling, urls)
	

	p.close()
	p.join()