from multiprocessing import Pool
from selenium import webdriver
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.support import expected_conditions as EC 
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By 
import mysql
import sys, mysql.connector
import re
import time
from sys import argv

def setUp(url,page):
    options = webdriver.ChromeOptions()
    options = options
    options.add_argument('--headless')
    options.add_argument("--log-level=3")
    driver = webdriver.Chrome(r'C:\GABI\TutorSelenium\tokpedmulti\chromedriver\chromedriver.exe',options=options)
    driver.implicitly_wait(30)
    driver.get(url)
    try:
        myElem = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, 'content-container')))
        print ("Page is ready!")
    except TimeoutException:
        print ("Loading took too much time!")
    #HALAMAN CRAWLING
    link = []
    page_number = 1
    page_maximum = int(page)
    while page_number<=page_maximum:
        try:
            link_page = driver.find_element_by_xpath("//a[contains(text(), "+str(page_number+1)+")]").get_attribute("href")
            # print(link_page)
        except NoSuchElementException:
            print("No page")
            break          
        jumlah_produk = driver.find_elements_by_xpath('//div[@class="_2p2-wGqG"]//div[@class="_33JN2R1i"]')
        # print(len(jumlah_produk))
        page = driver.find_elements_by_xpath('//div[@class="_27sG_y4O"]//a')
        
        for a in range(2):
        	nama = driver.find_elements_by_xpath('//div[@class="_27sG_y4O"]//a')[a].get_attribute("href")
        	# print(nama)
        	link.append(nama)
        
        # print(driver.current_url)
        driver.get(link_page)  
        page_number += 1
        # print("PAGE" + str(page_number))
    return link    
input_link = argv[1]
input_page = argv[2]     
setUp(input_link,input_page)
