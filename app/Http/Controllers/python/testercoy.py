from selenium import webdriver
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.support import expected_conditions as EC 
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By 
import selenium.webdriver.chrome.service as service
import time
import random

options = webdriver.ChromeOptions()
options = options
PROXY = ['20.186.6.199:3128','103.253.146.90'] # IP:PORT or HOST:PORT
options.add_argument('--headless')
options.add_argument("--log-level=3")
options.add_argument('--disable-dev-shm-usage')
options.add_argument('--no-sandbox')
options.add_argument('--proxy-server=https://%s' % random.choice(PROXY))
driver = webdriver.Chrome('/var/www/html/crawler/app/Http/Controllers/python/chromedriver/chromedriver',options=options)
driver.implicitly_wait(30)
driver.get("https://www.google.com/xhtml")
try:
	myElem = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, 'viewport')))
	print ("Page is ready!")
except TimeoutException:
	print ("Loading took too much time!")
print(driver.find_elements_by_xpath('//div[@class="gb_e gb_f"]//a[@class="gb_d"]'))
print("OK")

