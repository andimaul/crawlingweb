<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'produk';
}
