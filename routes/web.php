<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BerandaController@index')->name('home');
Route::get('/halo',function(){
	return view('halo');
});

Route::get('/logout', 'BerandaController@logout');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', 'BerandaController@index')->name('home');
// Route::get('/crawl/select/kategori', 'BerandaController@crawlkategori')->name('crawlkategori');
Route::get('/home/select', 'BerandaController@select')->name('filter');
Route::get('/home/select/kategori', 'BerandaController@kategori')->name('kategori');
Route::get('/home/select/kategori/subkategori', 'BerandaController@subkategori')->name('subkategori');
Route::get('/home/select/kategori/subkategori/subdetail', 'BerandaController@subdetail')->name('subdetail');
Route::post('/crawler', 'BerandaController@postcrawl')->name('postcrawl');
Route::get('/home/select/best-store', 'BerandaController@bestStore')->name('bestStore');
Route::get('/home/rate', 'BerandaController@getRate')->name('rate');
Route::get('/home/lokasi', 'BerandaController@getLokasi')->name('lokasi');
Route::get('/home/price/high', 'BerandaController@getHighPrice')->name('high');
Route::get('/home/price/min', 'BerandaController@getMinPrice')->name('min');
Route::get('/home/total', 'BerandaController@getTotalProduct')->name('total');
Route::get('/home/convert', 'BerandaController@getConvert')->name('convert');

Route::get('/crawl', 'BerandaController@crawl')->name('crawl');
Route::post('/crawl/crawling/store', 'BerandaController@crawlByStore')->name('crawlingstore');
Route::post('/crawl/crawling/keyword','BerandaController@crawlByKeyword')->name('crawlingkeyword');
Route::post('/crawl/crawling/category','BerandaController@crawlByMarketCategory')->name('crawlingcategory');
Route::get('crawl/select/kategori', 'BerandaController@selectKategori');
Route::get('crawl/select/kategori/subkategori', 'BerandaController@selectSubKategori');
Route::get('crawl/select/kategori/subkategori/subdetail', 'BerandaController@selectSubKategoriDetail');
